# ...

## Using

```bash
## Initial sync
rsync -av --no-owner --no-group --no-times --checksum --no-perms --ignore-existing \
    --exclude={.git,build*,mark,TEMPLATE-*.md} \
    ~/d/gitlab.com/aucampia/templates/asciidoctor/ ./ --dry-run

## Diff summary ...
diff -u --exclude={.git,build*,mark,TEMPLATE-*.md} -r -q \
    ~/d/gitlab.com/aucampia/templates/asciidoctor/ ./

## vimdiff
diff -u --exclude={.git,build*,mark,TEMPLATE-*.md} -r \
    ~/d/gitlab.com/aucampia/templates/asciidoctor/ ./ \
    | sed -E -n 's,^diff.* /,vimdiff /,gp'

## diff
diff -u --exclude={.git,build*,mark,TEMPLATE-*.md} -r \
    ~/d/gitlab.com/aucampia/templates/asciidoctor/ ./
```

